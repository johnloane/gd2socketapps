package basicudp;

import java.io.IOException;
import java.net.*;

public class Sender
{
    public static void main(String[] args)
    {
        DatagramSocket senderSocket = null;
        InetAddress receiverHost = null;
        try
        {
            receiverHost = Inet4Address.getLocalHost();
        }
        catch(UnknownHostException uhe)
        {
            System.out.println(uhe.getMessage());
        }
        int receiverPort = 50001;
        DatagramPacket datagram = null;
        try
        {
            senderSocket = initializeSenderSocket(senderSocket, receiverHost, receiverPort);
            datagram = initializeDatagramPacket(datagram, receiverHost, receiverPort);
            //Send the message to the other program
            senderSocket.send(datagram);

            System.out.println("Message sent!");

            getEchoResponseFromReceiver(senderSocket);
            Thread.sleep(8000);
        } catch (UnknownHostException uhe)
        {
            System.out.println(uhe.getMessage());
        } catch (SocketException se)
        {
            System.out.println(se.getMessage());
        } catch (InterruptedException ie)
        {
            System.out.println(ie.getMessage());
        } catch (IOException ioe)
        {
            System.out.println(ioe.getMessage());
        } finally
        {
            if (senderSocket != null)
            {
                senderSocket.close();
            }
        }
    }

    public static DatagramSocket initializeSenderSocket(DatagramSocket senderSocket, InetAddress receiverHost, int receiverPort)
    {
        try
        {
            //Create a port for the sender to listen on and receive into
            int senderPort = 50000;

            receiverPort = 50001;

            //Create a datagram socket for sending the data
            //bind it to a port - 50000
            senderSocket = new DatagramSocket(senderPort);
        }
        catch(SocketException se)
        {
            System.out.println(se.getMessage());
        }
        return senderSocket;
    }

    public static DatagramPacket initializeDatagramPacket(DatagramPacket datagram, InetAddress receiverHost, int receiverPort)
    {
        //Message to be sent
        String message = "Hello World. This is GD2 calling. Is there anyone out there?";
        //Get the information to be sent as a byte array
        byte buffer[] = message.getBytes();

        //Build the DatagramPacket
        //Needs
        // buffer: Data to be sent
        // buffer.length: how much data to send
        // receiverHost: IP address to send to
        // receiverPort: Port that the receiver is listening on
        datagram = new DatagramPacket(buffer, buffer.length, receiverHost, receiverPort);
        return datagram;
    }

    public static void getEchoResponseFromReceiver(DatagramSocket senderSocket)
    {
        byte[] response = new byte[70];
        DatagramPacket echo = new DatagramPacket(response, response.length);

        //Receive the responce
        //The senderSocket is blocking - it will wait for a response before it continues
        try
        {
            senderSocket.receive(echo);
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }

        //Get the content of the response
        String responseMessage = new String(echo.getData());

        //Display the received data
        System.out.println("Response: " + responseMessage);
    }
}








