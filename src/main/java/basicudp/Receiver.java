package basicudp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Receiver
{
    public static void main(String[] args)
    {
        int receiverPort = 50001;
        int senderPort = 50000;

        boolean continueRunning = true;

        final int MAX_LEN = 70;
        DatagramSocket receiverSocket = null;

        try
        {
            //Create a datagram for receiving data
            receiverSocket = new DatagramSocket(receiverPort);

            System.out.println("Waiting for message on port " + receiverPort + " ....");
            while(continueRunning)
            {
                byte buffer[] = new byte[MAX_LEN];

                DatagramPacket receivedData = new DatagramPacket(buffer, MAX_LEN);

                receiverSocket.receive(receivedData);

                //Convert the buffer of data into a string
                String message = new String(buffer);

                //Display the message
                System.out.println("Message received: " + message);

                //Figure out where the message came from
                InetAddress sender = receivedData.getAddress();
                System.out.println("Sender: " + sender);
                String responseMessage = message;

                byte[] responseArray = responseMessage.getBytes();
                //Build the packet and send it back
                DatagramPacket response = new DatagramPacket(responseArray, responseArray.length, sender, senderPort);

                receiverSocket.send(response);
            }
            Thread.sleep(8000);
        }
        catch(SocketException se)
        {
            System.out.println(se.getMessage());
        }
        catch(InterruptedException ie)
        {
            System.out.println(ie.getMessage());
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        finally
        {
            if(receiverSocket != null)
            {
                receiverSocket.close();
            }
        }

    }
}
